import { Component, OnInit } from '@angular/core';
import { CovidDataService } from '../covid-data.service';
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-pi-chart',
  templateUrl: './pi-chart.component.html',
  styleUrls: ['./pi-chart.component.css'],
})
export class PiChartComponent implements OnInit {
  updateFlag = false;
  Highcharts: typeof Highcharts = Highcharts;

  chartOptions: Highcharts.Options = {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie',
    },
    title: {
      text: '<strong>State Wise Confirmed Cases</strong>',
    },

    tooltip: {
      pointFormat: '{series.name}: <b>{point.y} %</b>',
    },

    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.y} %',
        },
      },
    },
    series: [
      {
        name: 'Confirmed Case',
        type: 'pie',
        data: [],
      },
    ],
  };

  constructor(private covidDataService: CovidDataService) {}

  ngOnInit(): void {
    this.covidDataService.getCovidData().subscribe((covidData: any[]) => {
      this.covidDataService
        .getCovidDataDistrictWise()
        .subscribe((districtWiseData) => {
          const stateName = Object.keys(districtWiseData).filter((item) => {
            if (item !== 'State Unassigned') {
              return districtWiseData[item];
            }
          });

          const newCovidData = Object.keys(covidData)
            .filter((item) => {
              if (item !== 'TT') {
                return covidData[item];
              }
            })
            .reduce((acc, key) => {
              acc[key] = covidData[key];
              return acc;
            }, {});

          Object.keys(newCovidData).forEach((item, index) => {
            this.chartOptions.series[0]['data'].push({
              name: stateName[index],
              y: Number(
                (
                  (newCovidData[item].total.confirmed /
                    covidData['TT'].total.confirmed) *
                  100
                ).toFixed(2)
              ),
            });
          });

          this.updateFlag = true;
        });
    });
  }
}
