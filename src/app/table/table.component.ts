import { Component, OnInit } from '@angular/core';
import { CovidDataService } from '../covid-data.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css'],
})
export class TableComponent implements OnInit {
  Object = Object;

  page = 1;
  pageSize = 6;
  collectionSize = 36;
  data: any;
  covidDataStateWise = [];

  covidData = [];

  refreshCountries() {
    this.covidData = Object.keys(this.covidDataStateWise)
      .map((state, i) => ({
        ...this.covidDataStateWise[state],
      }))
      .slice(
        (this.page - 1) * this.pageSize,
        (this.page - 1) * this.pageSize + this.pageSize
      );
  }

  constructor(private covidDataService: CovidDataService) {
    this.data = {
      stateSort: 0,
      confirmedSort: 0,
      activeSort: 0,
      recoveredSort: 0,
      deceasedSort: 0,
    };
  }

  onSort(sortingColumn, val) {
    if (val == 0) {
      this.data[sortingColumn + 'Sort'] = 1;

      this.covidDataStateWise = this.covidDataStateWise.sort((a, b) => {
        return a.total[sortingColumn] - b.total[sortingColumn];
      });
    } else {
      this.data[sortingColumn + 'Sort'] = 0;

      this.covidDataStateWise = this.covidDataStateWise.sort((a, b) => {
        return b.total[sortingColumn] - a.total[sortingColumn];
      });
    }

    this.covidData = this.covidDataStateWise;
    this.refreshCountries();
  }

  sortState(val) {
    if (val == 0) {
      this.data.stateSort = 1;

      this.covidDataStateWise = this.covidDataStateWise.sort((a, b) => {
        return a.state > b.state ? 1 : -1;
      });
    } else {
      this.data.stateSort = 0;

      this.covidDataStateWise = this.covidDataStateWise.sort((a, b) => {
        return b.state > a.state ? 1 : -1;
      });
    }

    this.covidData = this.covidDataStateWise;
    this.refreshCountries();
  }

  ngOnInit(): void {
    this.covidDataService.getCovidData().subscribe((data: any[]) => {
      this.covidDataStateWise = data;

      this.covidDataService
        .getCovidDataDistrictWise()
        .subscribe((districtWiseData) => {
          const stateName = Object.keys(districtWiseData);

          this.covidDataStateWise = Object.keys(this.covidDataStateWise)
            .filter((item) => {
              if (item !== 'TT') {
                return this.covidDataStateWise[item];
              }
            })
            .map((item, index) => {
              this.covidDataStateWise[item]['state'] = stateName[index + 1];

              if (
                !this.covidDataStateWise[item].total.hasOwnProperty('deceased')
              ) {
                this.covidDataStateWise[item].total['deceased'] = 0;
              }

              this.covidDataStateWise[item].total['active'] =
                this.covidDataStateWise[item].total.confirmed -
                (this.covidDataStateWise[item].total.recovered +
                  this.covidDataStateWise[item].total.deceased);

              return this.covidDataStateWise[item];
            })
            .sort((a, b) => b.total.confirmed - a.total.confirmed);

          this.covidData = this.covidDataStateWise;
          this.refreshCountries();
        });
    });
  }
}
