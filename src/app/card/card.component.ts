import { Component, OnInit } from '@angular/core';
import { CovidDataService } from '../covid-data.service';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css'],
})
export class CardComponent implements OnInit {
  covidDataStateWise = [];
  caseSummary = {};

  Object = Object;

  constructor(private covidDataService: CovidDataService) {}

  ngOnInit(): void {
    this.covidDataService.getCovidData().subscribe((data: any[]) => {
      this.covidDataStateWise = data;

      Object.keys(data).forEach((keys) => {
        if (keys === 'TT') {
          this.caseSummary['confirmed'] = data[keys].total.confirmed;
          this.caseSummary['recovered'] = data[keys].total.recovered;
          this.caseSummary['deceased'] = data[keys].total.deceased;
          this.caseSummary['active'] =
            data[keys].total.confirmed -
            (data[keys].total.recovered +
              data[keys].total.deceased +
              data[keys].total.other);
        }
      });
    });
  }
}
